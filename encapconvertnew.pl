#!/usr/bin/perl -Tw

# encapconvert.pl V0.2
# Script created by Antonio Querubin AH6BW tony@lavanauts.org
# Based on a script created by Jason Begley KY9J ky9j.com ky9j@arrl.net
# This script is used for converting the encap.txt file from the AMPR net
# into a loadable config file for use on Cisco or Juniper routers.
#

use Net::IP;

my ($line);
my %nets = ();		# IP object for each route.
my %gateways = ();	# Gateway for each route.
my %routes = ();	# List of routes for each gateway.
my %gw2tunnel = ();	# Tunnel ID for each gateway.
my %tunnel2gw = ();	# Tunnel ID to gateway map.
my $net = undef;
my $mask = undef;

#####
# Below are user defined varibles

my $loop = 'loopback0';		# LOOPBACK INT CHANGE IF ALREADY IN USE
my $picif = 'ip-0/2/0';		# Juniper Tunnel PIC
my $amprgw = '169.228.66.251';	# amprgw.sysnet.ucsd.edu

my $outip = '67.53.38.45';	# YOUR PUBLIC IP ADDRESS
my $loopip = '44.14.0.3';	# YOUR AMPR IP ADDRESS
my $type = 'linux';		# Type of router.

# EO user defined varibles
#####

my $file = $ARGV[0];
my $debug = $ARGV[1];
if(!$file) { usage(); exit; } 
if($file =~ /--help/) { usage(); exit; } 

open(ENCAP, $file);
@line = <ENCAP>;
close (ENCAP);
@line = grep (!/^\s*$/,@line);
@line = grep (!/^#/,@line);
chomp(@line);

foreach $line(@line) {
  ($junk, $junk, $p, $junk, $gw) = split(/\s+/, $line);
  $ip = new Net::IP $p || die;
 
  $prefix = $ip->prefix();
  $nets{$prefix} = $ip;
  $gateways{$prefix} = $gw;

  if ( defined($routes{$gw}) ) {
    push @{ $routes{$gw} }, $p;
    $tunnelid = $gw2tunnel{$gw};
  }
  else {
    $routes{$gw} = [ $p ];
    # Generate a unique Tunnel ID for each gateway address.  Junos 
    # interface unit values cannot exceed 16385 (~ 2**14).  So divide a 
    # 32-bit address by 18 bits.  Increment if it's not unique.
    $gwip = new Net::IP $gw || die;
    $gwint = $gwip->intip();
    $tunnelid = $gwint / (2**18);
    while (defined($tunnel2gw{$tunnelid})) {
      $tunnelid++;
    }
    $gw2tunnel{$gw} = $tunnelid;
    $tunnel2gw{$tunnelid} = $gw;
  }

  $n = $ip->ip();
  $s = $ip->prefixlen();
  $mask = $ip->mask();

  print "*ip info*\n";
  print "NET:$n\nBITS:$s MASK:$mask\nGW:$gw\nIF:$tunnelid\n\n";
}

foreach $tunnelid ( sort (keys %tunnel2gw) ) {
  print "$tunnel2gw{$tunnelid}: @{ $routes{ $tunnel2gw{$tunnelid} } }\n";
}

open (MYFILE, ">ampr-tunnel-config.txt");

if ($type eq 'cisco') {
  print MYFILE "interface $loop\n";
  print MYFILE " ip address $loopip 255.255.255.255\n\n";
  print MYFILE "interface Tunnel1\n";
  print MYFILE " description Default AMPRNet tunnel\n";
  print MYFILE " ip unnumbered $loop\n";
  print MYFILE " tunnel source $outip\n";
  print MYFILE " tunnel destination 169.228.66.251\n";
  print MYFILE " tunnel mode ipip\n";
  print MYFILE "end\n\n";
}
elsif ($type eq 'juniper') {
  print MYFILE "interfaces {\n";
  print MYFILE "    $picif {\n";
}
elsif ($type eq 'linux') {
  print MYFILE "ip tunnel add tunl0 mode ipip remote $amprgw local $outip\n";
  print MYFILE "ifconfig tunl0 up\n";
  print MYFILE "ip route add 44.0.0.0/8 via $amprgw dev tunl0 onlink\n";
}

# Generate the tunnel interfaces for all gateways except our own.

foreach $tunnelid (values %gw2tunnel) {

  $gw = $tunnel2gw{$tunnelid};  
  next if ($gw eq $outip);

  if (defined($debug)) {
    if ($type eq 'cisco') {
      print "\ninterface Tunnel$tunnelid\n";
      print " description Link to $gw\n";
      print " ip unnumbered $loop\n";
      print " tunnel source $outip\n";
      print " tunnel destination $gw\n";
      print " tunnel mode ipip\n";
    }
    elsif ($type eq 'juniper') {
      print "        unit $tunnelid {\n";
      print "            description \"Link to $gw\";\n";
      print "            tunnel {\n";
      print "                source $outip;\n";
      print "                destination $gw;\n";
      print "            }\n";
      print "            family inet;\n";
      print "        }\n";
    }
  }

  if ($type eq 'cisco') {
    print MYFILE "\ninterface Tunnel$tunnelid\n";
    print MYFILE " description Link to $gw\n";
    print MYFILE " ip unnumbered $loop\n";
    print MYFILE " tunnel source $outip\n";
    print MYFILE " tunnel destination $gw\n";
    print MYFILE " tunnel mode ipip\n";
  }
  elsif ($type eq 'juniper') {
    print MYFILE "        unit $tunnelid {\n";
    print MYFILE "            description \"Link to $gw\";\n";
    print MYFILE "            tunnel {\n";
    print MYFILE "                source $outip;\n";
    print MYFILE "                destination $gw;\n";
    print MYFILE "            }\n";
    print MYFILE "            family inet;\n";
    print MYFILE "        }\n";
  }
  elsif ($type eq 'linux') {
  }
}

if ($type eq 'cisco') {
  print MYFILE "\n";
}
elsif ($type eq 'juniper') {
  print MYFILE "    }\n";
  print MYFILE "}\n";
  print MYFILE "routing-options {\n";
  print MYFILE "    static {\n";
}

# Generate the static routes to each gateway.

foreach $prefix (keys %nets) {
  $ip = $nets{$prefix};
  $gw = $gateways{$prefix};
  next if ($gw eq $outip);
  
  $net = $ip->prefix();
  $n = $ip->ip();
  $mask = $ip->mask();

  $tunnelid = $gw2tunnel{$gw};

  if ($type eq 'cisco') {
    print MYFILE "ip route $n $mask tunnel$tunnelid\n";
  }
  elsif ($type eq 'juniper') {
    print MYFILE "        route $net next-hop $picif.$tunnelid;\n";
  }
  elsif ($type eq 'linux') {
    print MYFILE "ip route add $net via $gw dev tunl0 onlink\n";
  }
}

if ($type eq 'cisco') {
  print MYFILE "\nend\n";
}
elsif ($type eq 'juniper') {
  print MYFILE "    }\n";
  print MYFILE "}\n";
}

sub usage
  {
  print << "EOT";
*** This script is for creating a loadable config (copy tftp run) for   ***
*** cisco routers.  Please note that this was tested to work on 2651XM  ***
*** better, expect poor response on smaller/slower platforms.           ***
*** Edit this file and change varibles as noted to your values.         ***
*** File \"cisco-config.txt\" will be generated in this directory for     ***
*** tftp upload                                                         ***
*** Run as follows:                                                     ***
*** perl encapconvert.pl encap.txt                                      ***
EOT
  }
